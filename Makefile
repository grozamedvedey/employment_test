SRC=AttrVisitor.cc CommandLineOptions.cc LengthVisitor.cc main.cc ReverseVisitor.cc XMLParser.cc
TINYXML2_SRC=tinyxml2.cpp
TINYXML2_DIR=3rdparty/tinyxml2/

COMPILER=g++
EXEC_NAME=test-app

RES=${addprefix src/, ${SRC}} ${addprefix ${TINYXML2_DIR}, ${TINYXML2_SRC}}

all: build run

build:
	mkdir -p bin
	${COMPILER} ${RES} -I${TINYXML2_DIR} -o ./bin/${EXEC_NAME}

run: ./bin/${EXEC_NAME}
	./bin/${EXEC_NAME} --in note.xml --out bin/note1.xml --operation 1
	./bin/${EXEC_NAME} --in note.xml --out bin/note2.xml --operation 2

clean: ./bin/
	rm -R ./bin