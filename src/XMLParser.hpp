/**
*   @file: XMLParser.hpp
*   @author: Fedor Matantsev
*   @date: 23 August 2017 
*/

#pragma once

#include "AttrVisitor.hpp"

#include <string>
#include <tinyxml2.h>

// @brief builds xml and recursively traverses DOM using specified visitor
class XMLParser
{

public:

    XMLParser(const AttrVisitor::Ptr& i_visitor);

    // @brief loads document, parses and walks through it
    // @return returns false if an error occured, otherwise returns true
    bool ProcessXML(const std::string& i_path);

    // @brief writes processed XML
    // @return returns false if an error occured, otherwise returns true
    bool WriteXML(const std::string& i_path);

    std::string GetError();

private:

    tinyxml2::XMLDocument m_document;

    AttrVisitor::Ptr m_visitorPtr;

    std::string m_errorMsg;

};