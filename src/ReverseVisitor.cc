/**
*   @file: ReverseVisitor.cc
*   @author: Fedor Matantsev
*   @date: 23 August 2017 
*/

#include "ReverseVisitor.hpp"

#include <algorithm>

bool ReverseVisitor::VisitAttr(tinyxml2::XMLAttribute& io_attr)
{
    std::string value = io_attr.Value();
    std::reverse(value.begin(), value.end());
    io_attr.SetAttribute(value.c_str());
    return true;
}