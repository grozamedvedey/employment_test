/**
*   @file: ReverseVisitor.hpp
*   @author: Fedor Matantsev
*   @date: 23 August 2017 
*/

#pragma once

#include "AttrVisitor.hpp"

class ReverseVisitor : public AttrVisitor
{

public:

    // @brief called for every attribute. Note: documentation says "You should never change the document from a callback.",
    // but I've looked through the code and it turns out that we can EDIT attribute. Basically, we may change
    // CONTENTS, but not the STRUCTURE.
    // @return false if we don't need to continue traversal
    virtual bool VisitAttr(tinyxml2::XMLAttribute& io_attr) override;

};