/**
*   @file: main.cc
*   @author: Fedor Matantsev
*   @date: 22 August 2017 
*/

#include "CommandLineOptions.hpp"
#include "ReverseVisitor.hpp"
#include "LengthVisitor.hpp"
#include "XMLParser.hpp"

#include <iostream>
#include <cassert>

int main(int argc, char** argv)
{
    CommandLineOptions opts;
    opts.ParseCommandLine(argc, argv);

    std::string errorMsg;
    if (opts.GetError(errorMsg))
    {
        std::cout << errorMsg << std::endl;
        return 0;
    }

    AttrVisitor::Ptr visitor;

    switch(opts.GetOperation())
    {
        case EOperation::Reverse:
            visitor.reset(new ReverseVisitor);
            break;

        case EOperation::PrintLength:
            visitor.reset(new LengthVisitor);
            break;
        
        default:
            assert("Unhandled operation type");
    }

    XMLParser parser(visitor);
    if (!parser.ProcessXML(opts.GetInputFilePath()))
    {
        std::cout << parser.GetError() << std::endl;
        return 0;
    }

    if (!parser.WriteXML(opts.GetOutputFilePath()))
    {
        std::cout << parser.GetError() << std::endl;
        return 0;
    }

    std::cout << "Successfully finished." << std::endl;
    return 0;
}