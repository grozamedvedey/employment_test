/**
*   @file: AttrVisitor.hpp
*   @author: Fedor Matantsev
*   @date: 23 August 2017 
*/

#pragma once

#include <memory>
#include <tinyxml2.h>

// @brief used to process attributes while walking through the xml dom
class AttrVisitor : public tinyxml2::XMLVisitor
{

public:

    using Ptr = std::shared_ptr<AttrVisitor>;

    virtual bool VisitEnter(const tinyxml2::XMLElement& i_element, const tinyxml2::XMLAttribute* io_attr) override;

    // @brief called for every attribute. Note: documentation says "You should never change the document from a callback.",
    // but I've looked through the code and it turns out that we can EDIT attribute. Basically, we may change
    // CONTENTS, but not the STRUCTURE.
    // @return false if we don't need to continue traversal
    virtual bool VisitAttr(tinyxml2::XMLAttribute& io_attr);

};