/**
*   @file: CommandLineOptions.hpp
*   @author: Fedor Matantsev
*   @date: 23 August 2017 
*/

#pragma once

#include <getopt.h>
#include <vector>
#include <string>

enum class EOption
{
    In = 0,
    Out = 1,
    Operation = 2
};

enum class EOperation
{
    Unknown = 0,
    Reverse = 1,
    PrintLength = 2
};

class CommandLineOptions
{

public:

    CommandLineOptions();
    
    void ParseCommandLine(int i_argc, char** i_argv);

    // @brief returns true if there was a error during parsing, sends error message to o_msg
    bool GetError(std::string& o_msg);

    EOperation GetOperation();

    std::string GetInputFilePath();

    std::string GetOutputFilePath();

private:

    bool HandleOperationOption(const std::string& i_operation);

    // @brief This one keeps all the flags that we can handle
    std::vector<option> m_options;

    std::string m_inputFilePath;

    std::string m_outputFilePath;

    EOperation m_operation;

    // @brief Empty if there were no errors, otherwise contains error description
    std::string m_error;

};