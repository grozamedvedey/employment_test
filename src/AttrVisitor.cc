/**
*   @file: AttrVisitor.cc
*   @author: Fedor Matantsev
*   @date: 23 August 2017 
*/

#include "AttrVisitor.hpp"

#include <iostream>

bool AttrVisitor::VisitEnter(const tinyxml2::XMLElement& i_element, const tinyxml2::XMLAttribute* io_attr)
{
    while(io_attr)
    {
        tinyxml2::XMLAttribute* attr = const_cast<tinyxml2::XMLAttribute*>(io_attr);
        if (!attr || !VisitAttr(*attr))
        {
            return false;
        }
        io_attr = io_attr->Next();
    }
    return true;
}

bool AttrVisitor::VisitAttr(tinyxml2::XMLAttribute& io_attr)
{
    return true;
}