/**
*   @file: XMLParser.cc
*   @author: Fedor Matantsev
*   @date: 23 August 2017 
*/

#include "XMLParser.hpp"

XMLParser::XMLParser(const AttrVisitor::Ptr& i_visitor)
    : m_visitorPtr(i_visitor)
{
}

bool XMLParser::ProcessXML(const std::string& i_path)
{
    tinyxml2::XMLError result = m_document.LoadFile(i_path.c_str());

    if (result != tinyxml2::XML_SUCCESS)
    {
        m_errorMsg = "Bad input file";
        return false;
    }

    if (!m_visitorPtr)
    {
        m_errorMsg = "Visitor pointer is nullptr";
        return false;
    }
    
    m_document.Accept(m_visitorPtr.get());
    return true;
}

bool XMLParser::WriteXML(const std::string& i_path)
{
    tinyxml2::XMLError result = m_document.SaveFile(i_path.c_str());
    if (result != tinyxml2::XML_SUCCESS)
    {
        m_errorMsg = "Failed to write file";
        return false;
    }
    return true;
}

std::string XMLParser::GetError()
{
    return m_errorMsg;
}