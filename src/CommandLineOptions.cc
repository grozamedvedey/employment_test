/**
*   @file: CommandLineOptions.hpp
*   @author: Fedor Matantsev
*   @date: 23 August 2017 
*/

#include "CommandLineOptions.hpp"

#include <sstream>
#include <cassert>

CommandLineOptions::CommandLineOptions()
{
    m_options.push_back(option {
        /*name =*/      "in",
        /*has_arg =*/   required_argument,
        /*flag =*/      0,
        /*val =*/       static_cast<int>(EOption::In)
    });

    m_options.push_back(option {
        /*name =*/      "out",
        /*has_arg =*/   required_argument,
        /*flag =*/      0,
        /*val =*/       static_cast<int>(EOption::Out)
    });

    m_options.push_back(option {
        /*name =*/      "operation",
        /*has_arg =*/   required_argument,
        /*flag =*/      0,
        /*val =*/       static_cast<int>(EOption::Operation)
    });

    // Note: The last element of the array has to be filled with zeros.
    m_options.push_back(option{});
}

void CommandLineOptions::ParseCommandLine(int i_argc, char** i_argv)
{
    int opt = getopt_long(i_argc, i_argv, "", &m_options.front(), nullptr);

    while(opt != -1)
    {
        EOption option = static_cast<EOption>(opt);
        switch(option)
        {
            case EOption::In:
                m_inputFilePath = optarg;
                break;
            case EOption::Out:
                m_outputFilePath = optarg;
                break;
            case EOption::Operation:
                if (!HandleOperationOption(optarg))
                {
                    return;
                }
                break;
            default:
                // We shouldn't ever get here
                assert("Unhandled command line option.");
                break;
        }

        opt = getopt_long(i_argc, i_argv, "", &m_options.front(), nullptr);
    }
}

bool CommandLineOptions::HandleOperationOption(const std::string& i_value)
{
    size_t pos = 0;
    int op;

    try
    {
        op = std::stoi(i_value, &pos);
    }
    catch(const std::exception& e)
    {
        std::ostringstream os;
        os << "Bad operation type: " << i_value;
        m_error = os.str();
        return false;
    }

    if (pos != i_value.size())
    {
        std::ostringstream os;
        os << "Bad operation type: " << i_value;
        m_error = os.str();
        return false;
    }

    EOperation operation = static_cast<EOperation>(op);
    switch(operation)
    {
        case EOperation::Reverse:
        case EOperation::PrintLength:
            m_operation = operation;
            break;
        default:
            std::ostringstream os;
            os << "Bad operation type: " << i_value;
            m_error = os.str();
            return false;
            break;
    }

    return true;
}

bool CommandLineOptions::GetError(std::string& o_msg)
{
    o_msg = m_error;
    return !m_error.empty();
}

EOperation CommandLineOptions::GetOperation()
{
    return m_operation;
}

std::string CommandLineOptions::GetInputFilePath()
{
    return m_inputFilePath;
}

std::string CommandLineOptions::GetOutputFilePath()
{
    return m_outputFilePath;
}