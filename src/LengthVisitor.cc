/**
*   @file: LengthVisitor.cc
*   @author: Fedor Matantsev
*   @date: 23 August 2017 
*/

#include "LengthVisitor.hpp"

bool LengthVisitor::VisitAttr(tinyxml2::XMLAttribute& io_attr)
{
    const char* value = io_attr.Value();
    int length = strlen(value);
    io_attr.SetAttribute(length);
    return true;
}